﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjectConfiguration;
using System.ComponentModel;
using System.IO.Compression;

namespace Services
{
    public class ProjectService
    {
        public bool CreateNewCopy()
        {
            ProjectConfig config = new ProjectConfig();
            if (config.UserFilesStorePath == null)
            {
                string path;
                MessageBox.Show("The destination path is null! Please, select the folder which you want to backup.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                FolderBrowserDialog diag = new FolderBrowserDialog();

                if (diag.ShowDialog() == DialogResult.OK) //если открыто успешно - выполняем
                {
                    path = diag.SelectedPath;
                    config.SetUserStoreFolderPath(path);
                }
                else
                {
                    MessageBox.Show("Please, select the folder which you want to backup!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                string destPath = config.StoreFolderPath + DateTime.Now.ToString("dd.MM.yyyy hh.mm.ss");
                var check = DirectoryCopy(config.UserFilesStorePath, destPath, true);
                if(check == false)
                { 
                    MessageBox.Show("Chosen folder if empty! No files to backup", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                ZipFile.CreateFromDirectory(destPath, destPath + ".zip");
                Directory.Delete(destPath, true);
            }
            else
            {
                string destPath = config.StoreFolderPath + @"\\" + DateTime.Now.ToString("dd.MM.yyyy hh.mm.ss");
                var check = DirectoryCopy(config.UserFilesStorePath, config.StoreFolderPath + DateTime.Now.ToString(@"\\dd.MM.yyyy hh.mm.ss"), true);
                if (check == false)
                {
                    MessageBox.Show("Chosen folder if empty! No files to backup", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                ZipFile.CreateFromDirectory(destPath, destPath + ".zip", CompressionLevel.Optimal, true);
                Directory.Delete(destPath, true);
            }
            return true;
        }

        public bool DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            FileInfo[] files = dir.GetFiles();
            if (files.Length == 0)
            {
                return false;
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
           
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, true);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
            return true;
        }

        public void DeleteAllCopies()
        {
            ProjectConfig config = new ProjectConfig();
            var dir = new DirectoryInfo(config.StoreFolderPath);
            FileInfo[] files = dir.GetFiles();
            if(files.Length == 0)
            {
                MessageBox.Show("No copies to delete!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            foreach (var item in files)
            {
                item.Delete();
            }
            MessageBox.Show("Copies succesfuly deleted!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


    }
}

