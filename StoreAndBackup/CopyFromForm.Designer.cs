﻿namespace StoreAndBackup
{
    partial class CopyFromForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.footer_label = new System.Windows.Forms.Label();
            this.copies_label = new System.Windows.Forms.Label();
            this.button_copy_selected = new System.Windows.Forms.Button();
            this.list_of_copies = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // footer_label
            // 
            this.footer_label.AutoSize = true;
            this.footer_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.25F);
            this.footer_label.Location = new System.Drawing.Point(75, 427);
            this.footer_label.Name = "footer_label";
            this.footer_label.Size = new System.Drawing.Size(134, 12);
            this.footer_label.TabIndex = 15;
            this.footer_label.Text = "KHAI 2019 (c) Kirill Lukyanenko";
            // 
            // copies_label
            // 
            this.copies_label.AutoSize = true;
            this.copies_label.Location = new System.Drawing.Point(181, 33);
            this.copies_label.Name = "copies_label";
            this.copies_label.Size = new System.Drawing.Size(42, 13);
            this.copies_label.TabIndex = 14;
            this.copies_label.Text = "Copies:";
            // 
            // button_copy_selected
            // 
            this.button_copy_selected.Location = new System.Drawing.Point(15, 193);
            this.button_copy_selected.Name = "button_copy_selected";
            this.button_copy_selected.Size = new System.Drawing.Size(106, 42);
            this.button_copy_selected.TabIndex = 13;
            this.button_copy_selected.Text = "Restore";
            this.button_copy_selected.UseVisualStyleBackColor = true;
            this.button_copy_selected.Click += new System.EventHandler(this.Button_copy_selected_Click);
            // 
            // list_of_copies
            // 
            this.list_of_copies.FormattingEnabled = true;
            this.list_of_copies.Location = new System.Drawing.Point(142, 49);
            this.list_of_copies.Name = "list_of_copies";
            this.list_of_copies.Size = new System.Drawing.Size(150, 342);
            this.list_of_copies.TabIndex = 12;
            // 
            // CopyFromForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(307, 450);
            this.Controls.Add(this.footer_label);
            this.Controls.Add(this.copies_label);
            this.Controls.Add(this.button_copy_selected);
            this.Controls.Add(this.list_of_copies);
            this.Name = "CopyFromForm";
            this.Text = "Copy";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label footer_label;
        private System.Windows.Forms.Label copies_label;
        private System.Windows.Forms.Button button_copy_selected;
        private System.Windows.Forms.ListBox list_of_copies;
    }
}