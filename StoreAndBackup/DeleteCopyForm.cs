﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjectConfiguration;
namespace StoreAndBackup
{
    public partial class DeleteCopyForm : Form
    {
        public bool Flag { get; set; }
        public DeleteCopyForm()
        {
            InitializeComponent();
            ProjectConfig config = new ProjectConfig();
            Flag = true;

            if (!Directory.Exists(config.StoreFolderPath))
            { 
                MessageBox.Show("Please configure your project first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Flag = false;

                return;
            }
            else
            {
                var dir = new DirectoryInfo(config.StoreFolderPath);
                var files = dir.GetFiles();

                if (files.Length == 0)
                {
                    MessageBox.Show("No copies created! Create a copy and try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Flag = false;

                    return;
                }

                foreach (var item in files)
                {
                    list_of_copies.Items.Add(item.Name);
                }
            }
        }

        private void button_delete_copy_Сlick(object sender, EventArgs e)
        {
            if (list_of_copies.SelectedItem.ToString() == "No copies here.")
            {
                return;
            }
            ProjectConfig config = new ProjectConfig();
            if (list_of_copies.SelectedItem == null)
            { 
                MessageBox.Show("Please select copy first!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var file = new FileInfo(config.StoreFolderPath + "\\" + list_of_copies.SelectedItem.ToString());
            file.Delete();
            list_of_copies.Items.Clear();
            var tempDir = new DirectoryInfo(config.StoreFolderPath);
            var files = tempDir.GetFiles();
            if (files.Length == 0)
                list_of_copies.Items.Add("No copies here.");
            foreach (var item in files)
            {
                list_of_copies.Items.Add(item.Name);
            }
        }
    }
}
