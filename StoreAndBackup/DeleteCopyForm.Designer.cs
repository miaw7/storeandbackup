﻿namespace StoreAndBackup
{
    partial class DeleteCopyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.list_of_copies = new System.Windows.Forms.ListBox();
            this.button_delete_copy = new System.Windows.Forms.Button();
            this.Copies_label = new System.Windows.Forms.Label();
            this.footer_label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // list_of_copies
            // 
            this.list_of_copies.FormattingEnabled = true;
            this.list_of_copies.Location = new System.Drawing.Point(142, 31);
            this.list_of_copies.Name = "list_of_copies";
            this.list_of_copies.Size = new System.Drawing.Size(150, 342);
            this.list_of_copies.TabIndex = 0;
            // 
            // button_delete_copy
            // 
            this.button_delete_copy.Location = new System.Drawing.Point(15, 175);
            this.button_delete_copy.Name = "button_delete_copy";
            this.button_delete_copy.Size = new System.Drawing.Size(106, 42);
            this.button_delete_copy.TabIndex = 5;
            this.button_delete_copy.Text = "Delete";
            this.button_delete_copy.UseVisualStyleBackColor = true;
            this.button_delete_copy.Click += new System.EventHandler(this.button_delete_copy_Сlick);
            // 
            // Copies_label
            // 
            this.Copies_label.AutoSize = true;
            this.Copies_label.Location = new System.Drawing.Point(181, 15);
            this.Copies_label.Name = "Copies_label";
            this.Copies_label.Size = new System.Drawing.Size(42, 13);
            this.Copies_label.TabIndex = 6;
            this.Copies_label.Text = "Copies:";
            // 
            // footer_label
            // 
            this.footer_label.AutoSize = true;
            this.footer_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.25F);
            this.footer_label.Location = new System.Drawing.Point(75, 409);
            this.footer_label.Name = "footer_label";
            this.footer_label.Size = new System.Drawing.Size(134, 12);
            this.footer_label.TabIndex = 11;
            this.footer_label.Text = "KHAI 2019 (c) Kirill Lukyanenko";
            // 
            // DeleteCopyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(308, 430);
            this.Controls.Add(this.footer_label);
            this.Controls.Add(this.Copies_label);
            this.Controls.Add(this.button_delete_copy);
            this.Controls.Add(this.list_of_copies);
            this.Name = "DeleteCopyForm";
            this.Text = "Delete copy";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox list_of_copies;
        private System.Windows.Forms.Button button_delete_copy;
        private System.Windows.Forms.Label Copies_label;
        private System.Windows.Forms.Label footer_label;
    }
}