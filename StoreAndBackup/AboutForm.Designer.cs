﻿namespace StoreAndBackup
{
    partial class AboutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.about_head = new System.Windows.Forms.Label();
            this.course_label = new System.Windows.Forms.Label();
            this.made_by_label = new System.Windows.Forms.Label();
            this.author_label = new System.Windows.Forms.Label();
            this.name_label = new System.Windows.Forms.Label();
            this.button_exit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // about_head
            // 
            this.about_head.AutoSize = true;
            this.about_head.Location = new System.Drawing.Point(134, 384);
            this.about_head.Name = "about_head";
            this.about_head.Size = new System.Drawing.Size(59, 13);
            this.about_head.TabIndex = 0;
            this.about_head.Text = "KHAI 2019";
            // 
            // course_label
            // 
            this.course_label.AutoSize = true;
            this.course_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.course_label.Location = new System.Drawing.Point(100, 63);
            this.course_label.Name = "course_label";
            this.course_label.Size = new System.Drawing.Size(133, 24);
            this.course_label.TabIndex = 1;
            this.course_label.Text = "Course project";
            // 
            // made_by_label
            // 
            this.made_by_label.AutoSize = true;
            this.made_by_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.made_by_label.Location = new System.Drawing.Point(80, 188);
            this.made_by_label.Name = "made_by_label";
            this.made_by_label.Size = new System.Drawing.Size(164, 15);
            this.made_by_label.TabIndex = 2;
            this.made_by_label.Text = "Made by student 535B group";
            // 
            // author_label
            // 
            this.author_label.AutoSize = true;
            this.author_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.author_label.Location = new System.Drawing.Point(108, 227);
            this.author_label.Name = "author_label";
            this.author_label.Size = new System.Drawing.Size(119, 18);
            this.author_label.TabIndex = 3;
            this.author_label.Text = "Kirill Lukyanenko";
            // 
            // name_label
            // 
            this.name_label.AutoSize = true;
            this.name_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.name_label.Location = new System.Drawing.Point(99, 122);
            this.name_label.Name = "name_label";
            this.name_label.Size = new System.Drawing.Size(132, 17);
            this.name_label.TabIndex = 4;
            this.name_label.Text = "\"Store And Backup\"";
            // 
            // button_exit
            // 
            this.button_exit.Location = new System.Drawing.Point(112, 400);
            this.button_exit.Name = "button_exit";
            this.button_exit.Size = new System.Drawing.Size(102, 38);
            this.button_exit.TabIndex = 5;
            this.button_exit.Text = "Exit";
            this.button_exit.UseVisualStyleBackColor = true;
            this.button_exit.Click += new System.EventHandler(this.Button_exit_Click);
            // 
            // AboutForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(323, 450);
            this.Controls.Add(this.button_exit);
            this.Controls.Add(this.name_label);
            this.Controls.Add(this.author_label);
            this.Controls.Add(this.made_by_label);
            this.Controls.Add(this.course_label);
            this.Controls.Add(this.about_head);
            this.Name = "AboutForm";
            this.Text = "About";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label about_head;
        private System.Windows.Forms.Label course_label;
        private System.Windows.Forms.Label made_by_label;
        private System.Windows.Forms.Label author_label;
        private System.Windows.Forms.Label name_label;
        private System.Windows.Forms.Button button_exit;
    }
}