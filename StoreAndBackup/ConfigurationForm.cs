﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjectConfiguration;

namespace StoreAndBackup
{
    public partial class ConfigurationForm : Form
    {
        public ConfigurationForm()
        {
            InitializeComponent();
            ProjectConfig config = new ProjectConfig();
            program_store_textbox.Text = config.StoreFolderPath;
            user_store_textbox.Text = config.UserFilesStorePath;
        }

        private void Config_button_change_program_path_Click(object sender, EventArgs e)
        {
            ProjectConfig config = new ProjectConfig();

            string path;
            FolderBrowserDialog diag = new FolderBrowserDialog();

            if (diag.ShowDialog() == DialogResult.OK) //если открыто успешно - выполняем
            {
                path = diag.SelectedPath;
                config.SetProgramStoreFolderPath(path);
                program_store_textbox.Text = path;
            }
        }

        private void Config_button_change_user_path_Click(object sender, EventArgs e)
        {
            ProjectConfig config = new ProjectConfig();

            string path;
            FolderBrowserDialog diag = new FolderBrowserDialog();

            if (diag.ShowDialog() == DialogResult.OK) //если открыто успешно - выполняем
            {
                path = diag.SelectedPath;
                config.SetUserStoreFolderPath(path);
                user_store_textbox.Text = path;
            }
        }

        private void Config_button_default_settings_Click(object sender, EventArgs e)
        {
            if (File.Exists(Directory.GetCurrentDirectory() + @"\\Configuration\\file.xml"))
                File.Delete(Directory.GetCurrentDirectory() + @"\\Configuration\\file.xml");

            ProjectConfig config = new ProjectConfig();

            program_store_textbox.Text = config.StoreFolderPath;
            user_store_textbox.Text = config.UserFilesStorePath;
        }

        private void Config_button_exit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
