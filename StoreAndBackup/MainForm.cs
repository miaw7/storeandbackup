﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjectConfiguration;
using Services;

namespace StoreAndBackup
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void Create_new_copy_button_Click(object sender, EventArgs e)
        {
            var dialogResult = MessageBox.Show("Create new copy?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dialogResult == DialogResult.Yes)
            {
                ProjectService service = new ProjectService();
                service.CreateNewCopy();
                MessageBox.Show("Copy created!", "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dialogResult == DialogResult.No)
            {
                return;
            }

        }

        private void Configuration_button_Click(object sender, EventArgs e)
        {
            ConfigurationForm form = new ConfigurationForm();
            form.ShowDialog();
        }

        private void Exit_button_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                Application.Exit();
            }
            if (dialogResult == DialogResult.No)
            {
                return;
            }
        }

        private void Delete_copy_button_Click(object sender, EventArgs e)
        {
            DeleteCopyForm form = new DeleteCopyForm();
            if (form.Flag == false)
            {
                return;
            }
            form.ShowDialog();
        }

        private void Delete_all_button_Click(object sender, EventArgs e)
        {
            var dialogResult = MessageBox.Show("Are you sure?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dialogResult == DialogResult.Yes)
            {
                ProjectService service = new ProjectService();
                service.DeleteAllCopies();
                return;
            }
            if (dialogResult == DialogResult.No)
            {
                return;
            }

        }
        private void Restore_files_frm_button_Click(object sender, EventArgs e)
        {
            CopyFromForm form = new CopyFromForm();
            if (form.Flag == false)
            {
                return;
            }
            form.ShowDialog();
        }

        private void Restore_last_button_Click(object sender, EventArgs e)
        {
            var dialogResult = MessageBox.Show("Are you sure?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dialogResult == DialogResult.Yes)
            {
                ProjectConfig config = new ProjectConfig();
                ProjectService service = new ProjectService();

                if (config.UserFilesStorePath == null)
                {
                    string path;
                    MessageBox.Show("The destination path is null! Please, select the folder which you want to backup.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    FolderBrowserDialog diag = new FolderBrowserDialog();

                    if (diag.ShowDialog() == DialogResult.OK) //если открыто успешно - выполняем
                    {
                        path = diag.SelectedPath;
                        config.SetUserStoreFolderPath(path);
                    }
                    else
                    {
                        MessageBox.Show("Please, select the folder which you want to backup!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }

                var tempDir = new DirectoryInfo(config.StoreFolderPath);
                var files = tempDir.GetFiles();
                if(files.Length == 0)
                {
                    MessageBox.Show("No files to backup", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                DateTime? dateTime = null;
                string tempName = null;

                foreach (var item in files)
                {
                    if (dateTime == null)
                    {
                        dateTime = item.CreationTime;
                        tempName = item.Name;
                    }
                    else
                    {
                        if (dateTime < item.CreationTime)
                        {
                            dateTime = item.CreationTime;
                            tempName = item.Name;
                        }
                    }
                }
                string tempPath = config.StoreFolderPath + "\\" + tempName;


                string sourcePath = tempPath.Substring(0, tempPath.Length - 4);
                ZipFile.ExtractToDirectory(tempPath, config.StoreFolderPath);

                service.DirectoryCopy(sourcePath, config.UserFilesStorePath, true);
                Directory.Delete(sourcePath, true);
                MessageBox.Show("Files succesfully backuped!", "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if (dialogResult == DialogResult.No)
            {
                return;
            }
            
        }

        private void About_button_Click(object sender, EventArgs e)
        {
            AboutForm form = new AboutForm();
            form.ShowDialog();
        }
    }
}
