﻿using ProjectConfiguration;
using Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreAndBackup
{
    public partial class CopyFromForm : Form
    {
        public bool Flag { get; set; }
        public CopyFromForm()
        {
            InitializeComponent();
            ProjectConfig config = new ProjectConfig();
            Flag = true;

            if (!Directory.Exists(config.StoreFolderPath))
            {
                MessageBox.Show("Please configure your project first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Flag = false;

                return;
            }
            else
            {
                var dir = new DirectoryInfo(config.StoreFolderPath);
                var files = dir.GetFiles();

                if (files.Length == 0)
                {
                    MessageBox.Show("No copies created! Create a copy and try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Flag = false;

                    return;
                }

                foreach (var item in files)
                {
                    list_of_copies.Items.Add(item.Name);
                }
            }
        }

        private void Button_copy_selected_Click(object sender, EventArgs e)
        {
            ProjectConfig config = new ProjectConfig();
            ProjectService service = new ProjectService();
            if (list_of_copies.SelectedItem == null)
            {
                MessageBox.Show("Please select copy first!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        

            if (config.UserFilesStorePath == null)
            {
                string path;
                MessageBox.Show("The destination path is null! Please, select the folder which you want to backup.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                FolderBrowserDialog diag = new FolderBrowserDialog();

                if (diag.ShowDialog() == DialogResult.OK) //если открыто успешно - выполняем
                {
                    path = diag.SelectedPath;
                    config.SetUserStoreFolderPath(path);
                }
                else
                {
                    MessageBox.Show("Please, select the folder which you want to backup!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            string tempPath = config.StoreFolderPath + "\\" + list_of_copies.SelectedItem.ToString();

           
            string sourcePath = tempPath.Substring(0, tempPath.Length - 4);
            ZipFile.ExtractToDirectory(tempPath, config.StoreFolderPath);

            service.DirectoryCopy(sourcePath, config.UserFilesStorePath, true);
            Directory.Delete(sourcePath, true);
            MessageBox.Show("Files succesfully backuped!", "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information);
       
        }
           
        
    }
}
