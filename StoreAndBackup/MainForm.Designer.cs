﻿namespace StoreAndBackup
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.create_new_copy_button = new System.Windows.Forms.Button();
            this.restore_files_frm_button = new System.Windows.Forms.Button();
            this.configuration_button = new System.Windows.Forms.Button();
            this.restore_last_button = new System.Windows.Forms.Button();
            this.exit_button = new System.Windows.Forms.Button();
            this.about_button = new System.Windows.Forms.Button();
            this.delete_copy_button = new System.Windows.Forms.Button();
            this.delete_all_button = new System.Windows.Forms.Button();
            this.main_label = new System.Windows.Forms.Label();
            this.footer_label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // create_new_copy_button
            // 
            this.create_new_copy_button.Location = new System.Drawing.Point(20, 72);
            this.create_new_copy_button.Name = "create_new_copy_button";
            this.create_new_copy_button.Size = new System.Drawing.Size(106, 54);
            this.create_new_copy_button.TabIndex = 0;
            this.create_new_copy_button.Text = "Create a new copy";
            this.create_new_copy_button.UseVisualStyleBackColor = true;
            this.create_new_copy_button.Click += new System.EventHandler(this.Create_new_copy_button_Click);
            // 
            // restore_files_frm_button
            // 
            this.restore_files_frm_button.Location = new System.Drawing.Point(20, 194);
            this.restore_files_frm_button.Name = "restore_files_frm_button";
            this.restore_files_frm_button.Size = new System.Drawing.Size(106, 54);
            this.restore_files_frm_button.TabIndex = 1;
            this.restore_files_frm_button.Text = "Restore files from...";
            this.restore_files_frm_button.UseVisualStyleBackColor = true;
            this.restore_files_frm_button.Click += new System.EventHandler(this.Restore_files_frm_button_Click);
            // 
            // configuration_button
            // 
            this.configuration_button.Location = new System.Drawing.Point(20, 132);
            this.configuration_button.Name = "configuration_button";
            this.configuration_button.Size = new System.Drawing.Size(106, 54);
            this.configuration_button.TabIndex = 2;
            this.configuration_button.Text = "Configuration";
            this.configuration_button.UseVisualStyleBackColor = true;
            this.configuration_button.Click += new System.EventHandler(this.Configuration_button_Click);
            // 
            // restore_last_button
            // 
            this.restore_last_button.Location = new System.Drawing.Point(136, 132);
            this.restore_last_button.Name = "restore_last_button";
            this.restore_last_button.Size = new System.Drawing.Size(106, 54);
            this.restore_last_button.TabIndex = 3;
            this.restore_last_button.Text = "Restore last copy";
            this.restore_last_button.UseVisualStyleBackColor = true;
            this.restore_last_button.Click += new System.EventHandler(this.Restore_last_button_Click);
            // 
            // exit_button
            // 
            this.exit_button.Location = new System.Drawing.Point(20, 253);
            this.exit_button.Name = "exit_button";
            this.exit_button.Size = new System.Drawing.Size(222, 35);
            this.exit_button.TabIndex = 4;
            this.exit_button.Text = "Exit";
            this.exit_button.UseVisualStyleBackColor = true;
            this.exit_button.Click += new System.EventHandler(this.Exit_button_Click);
            // 
            // about_button
            // 
            this.about_button.Location = new System.Drawing.Point(69, 293);
            this.about_button.Name = "about_button";
            this.about_button.Size = new System.Drawing.Size(126, 29);
            this.about_button.TabIndex = 5;
            this.about_button.Text = "About";
            this.about_button.UseVisualStyleBackColor = true;
            this.about_button.Click += new System.EventHandler(this.About_button_Click);
            // 
            // delete_copy_button
            // 
            this.delete_copy_button.Location = new System.Drawing.Point(136, 72);
            this.delete_copy_button.Name = "delete_copy_button";
            this.delete_copy_button.Size = new System.Drawing.Size(106, 54);
            this.delete_copy_button.TabIndex = 6;
            this.delete_copy_button.Text = "Delete copy";
            this.delete_copy_button.UseVisualStyleBackColor = true;
            this.delete_copy_button.Click += new System.EventHandler(this.Delete_copy_button_Click);
            // 
            // delete_all_button
            // 
            this.delete_all_button.Location = new System.Drawing.Point(136, 194);
            this.delete_all_button.Name = "delete_all_button";
            this.delete_all_button.Size = new System.Drawing.Size(106, 54);
            this.delete_all_button.TabIndex = 7;
            this.delete_all_button.Text = "Delete all copies";
            this.delete_all_button.UseVisualStyleBackColor = true;
            this.delete_all_button.Click += new System.EventHandler(this.Delete_all_button_Click);
            // 
            // main_label
            // 
            this.main_label.AutoSize = true;
            this.main_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.main_label.Location = new System.Drawing.Point(60, 38);
            this.main_label.Name = "main_label";
            this.main_label.Size = new System.Drawing.Size(142, 20);
            this.main_label.TabIndex = 8;
            this.main_label.Text = "Store and Backup";
            // 
            // footer_label
            // 
            this.footer_label.AutoSize = true;
            this.footer_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.25F);
            this.footer_label.Location = new System.Drawing.Point(66, 339);
            this.footer_label.Name = "footer_label";
            this.footer_label.Size = new System.Drawing.Size(134, 12);
            this.footer_label.TabIndex = 9;
            this.footer_label.Text = "KHAI 2019 (c) Kirill Lukyanenko";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(262, 354);
            this.Controls.Add(this.footer_label);
            this.Controls.Add(this.main_label);
            this.Controls.Add(this.delete_all_button);
            this.Controls.Add(this.delete_copy_button);
            this.Controls.Add(this.about_button);
            this.Controls.Add(this.exit_button);
            this.Controls.Add(this.restore_last_button);
            this.Controls.Add(this.configuration_button);
            this.Controls.Add(this.restore_files_frm_button);
            this.Controls.Add(this.create_new_copy_button);
            this.Name = "MainForm";
            this.Text = "Store and Backup";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button create_new_copy_button;
        private System.Windows.Forms.Button restore_files_frm_button;
        private System.Windows.Forms.Button configuration_button;
        private System.Windows.Forms.Button restore_last_button;
        private System.Windows.Forms.Button exit_button;
        private System.Windows.Forms.Button about_button;
        private System.Windows.Forms.Button delete_copy_button;
        private System.Windows.Forms.Button delete_all_button;
        private System.Windows.Forms.Label main_label;
        private System.Windows.Forms.Label footer_label;
    }
}

