﻿namespace StoreAndBackup
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.program_store_textbox = new System.Windows.Forms.TextBox();
            this.config_button_change_program_path = new System.Windows.Forms.Button();
            this.configmain_label = new System.Windows.Forms.Label();
            this.user_store_textbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.config_button_change_user_path = new System.Windows.Forms.Button();
            this.config_button_default_settings = new System.Windows.Forms.Button();
            this.config_button_delete_all_data = new System.Windows.Forms.Button();
            this.config_button_exit = new System.Windows.Forms.Button();
            this.footer_label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Program store backups path";
            // 
            // program_store_textbox
            // 
            this.program_store_textbox.Location = new System.Drawing.Point(15, 91);
            this.program_store_textbox.Name = "program_store_textbox";
            this.program_store_textbox.Size = new System.Drawing.Size(285, 20);
            this.program_store_textbox.TabIndex = 1;
            // 
            // config_button_change_program_path
            // 
            this.config_button_change_program_path.Location = new System.Drawing.Point(303, 82);
            this.config_button_change_program_path.Name = "config_button_change_program_path";
            this.config_button_change_program_path.Size = new System.Drawing.Size(75, 36);
            this.config_button_change_program_path.TabIndex = 2;
            this.config_button_change_program_path.Text = "Change";
            this.config_button_change_program_path.UseVisualStyleBackColor = true;
            this.config_button_change_program_path.Click += new System.EventHandler(this.Config_button_change_program_path_Click);
            // 
            // configmain_label
            // 
            this.configmain_label.AutoSize = true;
            this.configmain_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.configmain_label.Location = new System.Drawing.Point(139, 19);
            this.configmain_label.Name = "configmain_label";
            this.configmain_label.Size = new System.Drawing.Size(121, 24);
            this.configmain_label.TabIndex = 3;
            this.configmain_label.Text = "Configuration";
            // 
            // user_store_textbox
            // 
            this.user_store_textbox.Location = new System.Drawing.Point(15, 163);
            this.user_store_textbox.Name = "user_store_textbox";
            this.user_store_textbox.Size = new System.Drawing.Size(285, 20);
            this.user_store_textbox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "User folder path";
            // 
            // config_button_change_user_path
            // 
            this.config_button_change_user_path.Location = new System.Drawing.Point(303, 154);
            this.config_button_change_user_path.Name = "config_button_change_user_path";
            this.config_button_change_user_path.Size = new System.Drawing.Size(75, 36);
            this.config_button_change_user_path.TabIndex = 6;
            this.config_button_change_user_path.Text = "Change";
            this.config_button_change_user_path.UseVisualStyleBackColor = true;
            this.config_button_change_user_path.Click += new System.EventHandler(this.Config_button_change_user_path_Click);
            // 
            // config_button_default_settings
            // 
            this.config_button_default_settings.Location = new System.Drawing.Point(15, 228);
            this.config_button_default_settings.Name = "config_button_default_settings";
            this.config_button_default_settings.Size = new System.Drawing.Size(165, 36);
            this.config_button_default_settings.TabIndex = 7;
            this.config_button_default_settings.Text = "Default settings";
            this.config_button_default_settings.UseVisualStyleBackColor = true;
            this.config_button_default_settings.Click += new System.EventHandler(this.Config_button_default_settings_Click);
            // 
            // config_button_delete_all_data
            // 
            this.config_button_delete_all_data.Location = new System.Drawing.Point(214, 228);
            this.config_button_delete_all_data.Name = "config_button_delete_all_data";
            this.config_button_delete_all_data.Size = new System.Drawing.Size(164, 36);
            this.config_button_delete_all_data.TabIndex = 8;
            this.config_button_delete_all_data.Text = "Delete all store data";
            this.config_button_delete_all_data.UseVisualStyleBackColor = true;
            // 
            // config_button_exit
            // 
            this.config_button_exit.Location = new System.Drawing.Point(116, 280);
            this.config_button_exit.Name = "config_button_exit";
            this.config_button_exit.Size = new System.Drawing.Size(144, 47);
            this.config_button_exit.TabIndex = 9;
            this.config_button_exit.Text = "Exit";
            this.config_button_exit.UseVisualStyleBackColor = true;
            this.config_button_exit.Click += new System.EventHandler(this.Config_button_exit_Click);
            // 
            // footer_label
            // 
            this.footer_label.AutoSize = true;
            this.footer_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.25F);
            this.footer_label.Location = new System.Drawing.Point(126, 358);
            this.footer_label.Name = "footer_label";
            this.footer_label.Size = new System.Drawing.Size(134, 12);
            this.footer_label.TabIndex = 10;
            this.footer_label.Text = "KHAI 2019 (c) Kirill Lukyanenko";
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(393, 379);
            this.Controls.Add(this.footer_label);
            this.Controls.Add(this.config_button_exit);
            this.Controls.Add(this.config_button_delete_all_data);
            this.Controls.Add(this.config_button_default_settings);
            this.Controls.Add(this.config_button_change_user_path);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.user_store_textbox);
            this.Controls.Add(this.configmain_label);
            this.Controls.Add(this.config_button_change_program_path);
            this.Controls.Add(this.program_store_textbox);
            this.Controls.Add(this.label1);
            this.Name = "ConfigurationForm";
            this.Text = "Configuration";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox program_store_textbox;
        private System.Windows.Forms.Button config_button_change_program_path;
        private System.Windows.Forms.Label configmain_label;
        private System.Windows.Forms.TextBox user_store_textbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button config_button_change_user_path;
        private System.Windows.Forms.Button config_button_default_settings;
        private System.Windows.Forms.Button config_button_delete_all_data;
        private System.Windows.Forms.Button config_button_exit;
        private System.Windows.Forms.Label footer_label;
    }
}