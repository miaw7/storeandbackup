﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ProjectConfiguration
{
    public class ProjectConfig
    {
        public string StoreFolderPath { get; set; }
        public string UserFilesStorePath { get; set; }

        public ProjectConfig()
        {
            var xmlStruct = new XmlConfigurationStruct();
            if (!Directory.Exists(Directory.GetCurrentDirectory() + @"\\Configuration\\"))
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\\Configuration\\");

            string path = Directory.GetCurrentDirectory() + @"\\Configuration\\file.xml";

            FileStream fs = new FileStream(path, FileMode.OpenOrCreate);
            XmlSerializer serializer = new XmlSerializer(typeof(XmlConfigurationStruct));

            if (!Directory.Exists(Directory.GetCurrentDirectory() + @"\\Main Storage\\"))
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\\Main Storage\\");
            if (fs.Length == 0)
            {
                xmlStruct.StoreFilesPath = Directory.GetCurrentDirectory() + @"\\Main Storage\\";
                serializer.Serialize(fs, xmlStruct);
                fs.Close();
                StoreFolderPath = xmlStruct.StoreFilesPath;
                UserFilesStorePath = null;
            }
            else
            {
                xmlStruct = serializer.Deserialize(fs) as XmlConfigurationStruct;
                StoreFolderPath = xmlStruct.StoreFilesPath;
                UserFilesStorePath = xmlStruct.UserFilesPath;
                fs.Close();
            }
        }

        public void SetUserStoreFolderPath(string path)
        {
            string configPath = Directory.GetCurrentDirectory() + @"\\Configuration\\file.xml";
            var xmlStruct = new XmlConfigurationStruct();
            FileStream fs = new FileStream(configPath, FileMode.Create);
            XmlSerializer serializer = new XmlSerializer(typeof(XmlConfigurationStruct));
            xmlStruct.StoreFilesPath = StoreFolderPath;
            xmlStruct.UserFilesPath = path;
            serializer.Serialize(fs, xmlStruct);
            fs.Close();
            UserFilesStorePath = path;
        }

        public void SetProgramStoreFolderPath(string path)
        {
            string configPath = Directory.GetCurrentDirectory() + @"\\Configuration\\file.xml";
            var xmlStruct = new XmlConfigurationStruct();
            FileStream fs = new FileStream(configPath, FileMode.Create);
            XmlSerializer serializer = new XmlSerializer(typeof(XmlConfigurationStruct));
            xmlStruct.StoreFilesPath = path;
            xmlStruct.UserFilesPath = UserFilesStorePath;
            serializer.Serialize(fs, xmlStruct);
            fs.Close();
            StoreFolderPath = path;
        }
    }
}
