﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectConfiguration
{
    [Serializable]
    public class XmlConfigurationStruct
    {
        public string StoreFilesPath { get; set; }
        public string UserFilesPath { get; set; }

    }
}
